import '../utilities/app_theme.dart';
import '../utilities/app_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class InfoTealWidget extends StatefulWidget {
  const InfoTealWidget({
    Key key,
    this.title,
    this.value,
  }) : super(key: key);

  final String title;
  final String value;

  @override
  _InfoTealWidgetState createState() => _InfoTealWidgetState();
}

class _InfoTealWidgetState extends State<InfoTealWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(16, 15, 16, 15),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.title,
                style: BitSportsTheme.title3,
              ),
              Text(
                widget.value,
                style: BitSportsTheme.title2,
              ),
            ],
          ),
        ),
        Divider(
          height: 3,
          thickness: 1,
          indent: 16,
          color: Color(0x19000000),
        ),
      ],
    );
  }
}
