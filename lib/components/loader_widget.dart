import '../utilities/app_theme.dart';
import '../utilities/app_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class LoaderWidget extends StatefulWidget {
  const LoaderWidget({Key key}) : super(key: key);

  @override
  _LoaderWidgetState createState() => _LoaderWidgetState();
}

class _LoaderWidgetState extends State<LoaderWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SpinKitFadingCircle(
          color: Color(0xFFDADADA),
          size: 50,
        ),
        Padding(
          padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
          child: Text(
            'Loading',
            style: BitSportsTheme.title3,
          ),
        ),
      ],
    );
  }
}
