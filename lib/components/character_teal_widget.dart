import '../backend/api_requests/api_calls.dart';
import '../character_page/character_page_widget.dart';
import '../utilities/app_theme.dart';
import '../utilities/app_util.dart';
import '../utilities/custom_functions.dart' as functions;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CharacterTealWidget extends StatefulWidget {
  const CharacterTealWidget({
    Key key,
    this.characterJson,
  }) : super(key: key);

  final dynamic characterJson;

  @override
  _CharacterTealWidgetState createState() => _CharacterTealWidgetState();
}

class _CharacterTealWidgetState extends State<CharacterTealWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        await Navigator.push(
          context,
          PageTransition(
            type: PageTransitionType.rightToLeft,
            duration: Duration(milliseconds: 300),
            reverseDuration: Duration(milliseconds: 300),
            child: CharacterPageWidget(
              characterJson: widget.characterJson,
            ),
          ),
        );
      },
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          Padding(
            padding: EdgeInsetsDirectional.fromSTEB(0, 16, 0, 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(16, 0, 0, 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        valueOrDefault<String>(
                          getJsonField(
                            widget.characterJson,
                            r'''$.name''',
                          ).toString(),
                          'Nombre',
                        ),
                        style: BitSportsTheme.title2,
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          FutureBuilder<ApiCallResponse>(
                            future: GetSpecieCall.call(
                              index:
                                  functions.getUrlIndex(valueOrDefault<String>(
                                getJsonField(
                                  widget.characterJson,
                                  r'''$.species''',
                                ).toString(),
                                'https://swapi.dev/api/species/1/',
                              )),
                            ),
                            builder: (context, snapshot) {
                              // Customize what your widget looks like when it's loading.
                              if (!snapshot.hasData) {
                                return Container();
                              }
                              final textGetSpecieResponse = snapshot.data;
                              return Text(
                                valueOrDefault<String>(
                                  getJsonField(
                                    textGetSpecieResponse.jsonBody,
                                    r'''$.name''',
                                  ).toString(),
                                  'specie',
                                ),
                                style: BitSportsTheme.bodyText2,
                              );
                            },
                          ),
                          Text(
                            ' from ',
                            style: BitSportsTheme.bodyText2,
                          ),
                          FutureBuilder<ApiCallResponse>(
                            future: GetPlanetCall.call(
                              index: functions.getUrlIndex(getJsonField(
                                widget.characterJson,
                                r'''$.homeworld''',
                              ).toString()),
                            ),
                            builder: (context, snapshot) {
                              // Customize what your widget looks like when it's loading.
                              if (!snapshot.hasData) {
                                return Container();
                              }
                              final textGetPlanetResponse = snapshot.data;
                              return Text(
                                valueOrDefault<String>(
                                  GetPlanetCall.planetName(
                                    textGetPlanetResponse.jsonBody,
                                  ).toString(),
                                  'homeworld',
                                ),
                                style: BitSportsTheme.bodyText2,
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(0, 0, 16, 0),
                  child: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black,
                    size: 19,
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 3,
            thickness: 1,
            indent: 16,
            color: Color(0x19000000),
          ),
        ],
      ),
    );
  }
}
