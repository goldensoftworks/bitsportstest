import '../utilities/app_theme.dart';
import '../utilities/app_util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class FailLoadDataWidget extends StatefulWidget {
  const FailLoadDataWidget({Key key}) : super(key: key);

  @override
  _FailLoadDataWidgetState createState() => _FailLoadDataWidgetState();
}

class _FailLoadDataWidgetState extends State<FailLoadDataWidget> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: AlignmentDirectional(0, 0),
      child: Text(
        'Failed to Load Data',
        style: BitSportsTheme.title3.override(
          fontFamily: 'sf pro display',
          color: Color(0xFFEC5757),
          useGoogleFonts: false,
        ),
      ),
    );
  }
}
