import '../../utilities/app_util.dart';

import 'api_manager.dart';

export 'api_manager.dart' show ApiCallResponse;

class GetPeopleCall {
  static Future<ApiCallResponse> call({
    int page = 1,
  }) {
    return ApiManager.instance.makeApiCall(
      callName: 'getPeople',
      apiUrl: 'https://swapi.dev/api/people',
      callType: ApiCallType.GET,
      headers: {},
      params: {
        'page': page,
      },
      returnBody: true,
    );
  }

  static dynamic peopleList(dynamic response) => getJsonField(
        response,
        r'''$.results''',
      );

  static dynamic nextPage(dynamic response) => getJsonField(
    response,
    r'''$.next''',
  );
}

class GetSpecieCall {
  static Future<ApiCallResponse> call({
    int index = 1,
  }) {
    return ApiManager.instance.makeApiCall(
      callName: 'getSpecie',
      apiUrl: 'https://swapi.dev/api/species/${index}/',
      callType: ApiCallType.GET,
      headers: {},
      params: {},
      returnBody: true,
    );
  }

  static dynamic specieName(dynamic response) => getJsonField(
        response,
        r'''$.name''',
      );
}

class GetPlanetCall {
  static Future<ApiCallResponse> call({
    int index = 1,
  }) {
    return ApiManager.instance.makeApiCall(
      callName: 'getPlanet',
      apiUrl: 'https://swapi.dev/api/planets/${index}/',
      callType: ApiCallType.GET,
      headers: {},
      params: {},
      returnBody: true,
    );
  }

  static dynamic planetName(dynamic response) => getJsonField(
        response,
        r'''$.name''',
      );
}

class GetVehicleCall {
  static Future<ApiCallResponse> call({
    int index = 1,
  }) {
    return ApiManager.instance.makeApiCall(
      callName: 'getVehicle',
      apiUrl: 'https://swapi.dev/api/vehicles/${index}/',
      callType: ApiCallType.GET,
      headers: {},
      params: {},
      returnBody: true,
    );
  }

  static dynamic vehicleName(dynamic response) => getJsonField(
        response,
        r'''$.name''',
      );
}
