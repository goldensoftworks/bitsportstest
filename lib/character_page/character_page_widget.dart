import '../backend/api_requests/api_calls.dart';
import '../components/info_teal_widget.dart';
import '../utilities/app_theme.dart';
import '../utilities/app_util.dart';
import '../utilities/custom_functions.dart' as functions;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class CharacterPageWidget extends StatefulWidget {
  const CharacterPageWidget({
    Key key,
    this.characterJson,
  }) : super(key: key);

  final dynamic characterJson;

  @override
  _CharacterPageWidgetState createState() => _CharacterPageWidgetState();
}

class _CharacterPageWidgetState extends State<CharacterPageWidget> {
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: BitSportsTheme.primaryColor,
        iconTheme: IconThemeData(color: Colors.white),
        automaticallyImplyLeading: true,
        title: Text(
          getJsonField(
            widget.characterJson,
            r'''$.name''',
          ).toString(),
          style: BitSportsTheme.title1,
        ),
        actions: [],
        centerTitle: true,
        elevation: 4,
      ),
      backgroundColor: Color(0xFFF5F5F5),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(16, 32, 16, 8),
              child: Text(
                'General information',
                style: BitSportsTheme.title2,
              ),
            ),
            InfoTealWidget(
              title: 'Eye Color',
              value: getJsonField(
                widget.characterJson,
                r'''$.eye_color''',
              ).toString(),
            ),
            InfoTealWidget(
              title: 'Hair Color',
              value: getJsonField(
                widget.characterJson,
                r'''$.hair_color''',
              ).toString(),
            ),
            InfoTealWidget(
              title: 'Skin Color',
              value: getJsonField(
                widget.characterJson,
                r'''$.skin_color''',
              ).toString(),
            ),
            InfoTealWidget(
              title: 'Birth Year',
              value: getJsonField(
                widget.characterJson,
                r'''$.birth_year''',
              ).toString(),
            ),
            Padding(
              padding: EdgeInsetsDirectional.fromSTEB(16, 32, 16, 8),
              child: Text(
                'Vehicles',
                style: BitSportsTheme.title2,
              ),
            ),
            Builder(
              builder: (context) {
                final vehiclesList = getJsonField(
                      widget.characterJson,
                      r'''$.vehicles''',
                    )?.toList() ??
                    [];
                return SingleChildScrollView(
                  primary: false,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children:
                        List.generate(vehiclesList.length, (vehiclesListIndex) {
                      final vehiclesListItem = vehiclesList[vehiclesListIndex];
                      return Container(
                        width: double.infinity,
                        decoration: BoxDecoration(),
                        child: FutureBuilder<ApiCallResponse>(
                          future: GetVehicleCall.call(
                            index: functions.getUrlIndex(getJsonField(
                              vehiclesListItem,
                              r'''$''',
                            ).toString()),
                          ),
                          builder: (context, snapshot) {
                            // Customize what your widget looks like when it's loading.
                            if (!snapshot.hasData) {
                              return Center(
                                child: SizedBox(
                                  width: 50,
                                  height: 50,
                                  child: SpinKitFadingCircle(
                                    color: Color(0xFFDADADA),
                                    size: 50,
                                  ),
                                ),
                              );
                            }
                            final infoTealGetVehicleResponse = snapshot.data;
                            return InfoTealWidget(
                              title: valueOrDefault<String>(
                                getJsonField(
                                  infoTealGetVehicleResponse.jsonBody,
                                  r'''$.name''',
                                ).toString(),
                                'Nombre',
                              ),
                              value: ' ',
                            );
                          },
                        ),
                      );
                    }),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
