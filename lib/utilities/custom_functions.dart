import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'lat_lng.dart';
import 'place.dart';

int getUrlIndex(String url) {
  // get number from the string
  if (url == null) return null;
  if (url == "[]") return 1;
  final _regex = RegExp(r'\d+');
  return int.tryParse(_regex.stringMatch(url));
}
