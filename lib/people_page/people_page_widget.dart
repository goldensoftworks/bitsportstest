import '../backend/api_requests/api_calls.dart';
import '../components/character_teal_widget.dart';
import '../components/loader_widget.dart';
import '../utilities/app_theme.dart';
import 'package:flutter/material.dart';
import '../utilities/custom_functions.dart' as functions;

class PeoplePageWidget extends StatefulWidget {
  @override
  _PeoplePageWidgetState createState() => _PeoplePageWidgetState();
}

class _PeoplePageWidgetState extends State<PeoplePageWidget> {
  ApiCallResponse apiCallOutput;
  List<dynamic> peopleList = [];
  int pageIndex = 1;
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: BitSportsTheme.primaryColor,
        automaticallyImplyLeading: false,
        title: Text(
          'People of Star Wars',
          style: BitSportsTheme.title1.override(
            fontFamily: 'sf pro display',
            color: Colors.white,
            fontSize: 17,
            fontWeight: FontWeight.bold,
            useGoogleFonts: false,
          ),
        ),
        actions: [],
        centerTitle: true,
        elevation: 4,
      ),
      backgroundColor: Color(0xFFF5F5F5),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              FutureBuilder<ApiCallResponse>(
                future: GetPeopleCall.call(),
                builder: (context, snapshot) {
                  // Customize what your widget looks like when it's loading.
                  if (!snapshot.hasData) {
                    return Center(
                      child: SizedBox(
                        width: 50,
                        height: 50,
                        child: Container(),
                      ),
                    );
                  }
                  final listViewGetPeopleResponse = snapshot.data;
                  return Builder(
                    builder: (context) {
                      if (peopleList.isEmpty) {
                        peopleList = GetPeopleCall.peopleList(
                              listViewGetPeopleResponse.jsonBody,
                            )?.toList() ??
                            [];
                        pageIndex = functions.getUrlIndex(
                            GetPeopleCall.nextPage(
                                listViewGetPeopleResponse.jsonBody));
                      }
                      return ListView.builder(
                        padding: EdgeInsets.zero,
                        primary: false,
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: peopleList.length,
                        itemBuilder: (context, peopleListIndex) {
                          final peopleListItem = peopleList[peopleListIndex];
                          return CharacterTealWidget(
                            characterJson: peopleListItem,
                          );
                        },
                      );
                    },
                  );
                },
              ),
              Visibility(
                visible: pageIndex != null,
                child: Padding(
                  padding: EdgeInsetsDirectional.fromSTEB(8, 8, 8, 8),
                  child: InkWell(
                    onTap: () async {
                      // Get a JSON with people from the page
                      apiCallOutput = await GetPeopleCall.call(
                        page: pageIndex,
                      );
                      // If the call is successful, it saves the next page index
                      // and adds new people to the list
                      if (apiCallOutput.succeeded) {
                        pageIndex = functions.getUrlIndex(
                            GetPeopleCall.nextPage(apiCallOutput.jsonBody));

                        GetPeopleCall.peopleList(
                          apiCallOutput.jsonBody,
                        )?.toList()?.forEach((e) {
                          peopleList.add(e);
                        });
                      } else {
                        // If the call fails, it shows a message
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(
                              'Failed to Load Data',
                              style: BitSportsTheme.title3.override(
                                fontFamily: 'sf pro display',
                                color: Color(0xFFEC5757),
                                useGoogleFonts: false,
                              ),
                            ),
                            duration: Duration(milliseconds: 4000),
                            backgroundColor: BitSportsTheme.primaryColor,
                          ),
                        );
                      }

                      setState(() {});
                    },
                    child: LoaderWidget(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
